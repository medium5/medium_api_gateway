package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/medium5/medium_api_gateway/api/models"
	pbc "gitlab.com/medium5/medium_api_gateway/genproto/post_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// @Security ApiKeyAuth
// @Router /comments [post]
// @Summary Create a comment
// @Description Create a comment
// @Tags comment
// @Accept json
// @Produce json
// @Param comment body models.CreateCommentRequest true "comment"
// @Success 201 {object} models.Comment
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateComment(c *gin.Context) {
	var req models.Comment

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	payload, err := h.GetAuthPayload(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	resp, err := h.grpcClient.CommentService().Create(context.Background(), &pbc.Comment{
		UserId:      payload.UserID,
		PostId:      req.PostID,
		Description: req.Description,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseCommentModel(resp))
}

func parseCommentModel(com *pbc.Comment) models.Comment {
	return models.Comment{
		ID:          com.Id,
		UserID:      com.UserId,
		PostID:      com.PostId,
		Description: com.Description,
		CreatedAt:   com.CreatedAt,
		UpdatedAt:   com.UpdatedAt,
		User: models.CommentUser{
			FirstName:       com.User.FirstName,
			LastName:        com.User.LastName,
			Email:           com.User.Email,
			ProfileImageUrl: com.User.ProfileImage,
		},
	}
}

// @Security ApiKeyAuth
// @Router /comments/{id} [get]
// @Summary Get comment by id
// @Description Get comment by id
// @Tags comment
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.Comment
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetComment(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	payload, err := h.GetAuthPayload(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	resp, err := h.grpcClient.CommentService().Get(context.Background(), &pbc.GetComment{Id: int64(id), UserId: payload.UserID})
	if err != nil {
		h.logger.WithError(err).Error("failed to get comment")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, parseCommentModel(resp))
}

// @Security ApiKeyAuth
// @Router /comments [get]
// @Summary Get all comment
// @Description Get all comment
// @Tags comment
// @Accept json
// @Produce json
// @Param filter query models.GetAllCommentsRequest false "Filter"
// @Success 200 {object} models.GetAllCommentsResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllComments(c *gin.Context) {
	req, err := validateGetAllCommentsParams(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	res, err := h.grpcClient.CommentService().GetAll(context.Background(), &pbc.GetAllCommentsRequest{
		Page:   req.Page,
		Limit:  req.Limit,
		UserId: req.UserID,
		PostId: req.PostID,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to get all comments")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, getCommentsResponse(res))
}

func getCommentsResponse(data *pbc.GetAllCommentsResponse) *models.GetAllCommentsResponse {
	resp := models.GetAllCommentsResponse{
		Comments: make([]*models.Comment, 0),
		Count:    data.Count,
	}

	for _, com := range data.Comments {
		comment := parseCommentModel(com)
		resp.Comments = append(resp.Comments, &comment)
	}

	return &resp
}

// @Security ApiKeyAuth
// @Router /comments/{id} [put]
// @Summary Update a comment
// @Description Update a comment
// @Tags comment
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param comment body models.UpdateCommentRequest true "Comment"
// @Success 200 {object} models.Comment
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdateComment(c *gin.Context) {
	var (
		req models.Comment
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	payload, err := h.GetAuthPayload(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	com, err := h.grpcClient.CommentService().Update(context.Background(), &pbc.Comment{
		Id:          id,
		UserId:      payload.UserID,
		Description: req.Description,
		UpdatedAt:   req.UpdatedAt,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to update post")
		if s, _ := status.FromError(err); s.Code() == codes.NotFound {
			c.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, parseCommentModel(com))
}

// @Security ApiKeyAuth
// @Router /comments/{id} [delete]
// @Summary Delete comment
// @Description Delete comment
// @Tags comment
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteComment(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	_, err = h.grpcClient.CommentService().Delete(context.Background(), &pbc.GetComment{Id: int64(id)})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, models.ResponseOK{
		Message: "Successfully deleted!",
	})

}
