package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/medium5/medium_api_gateway/api/models"
	pbl "gitlab.com/medium5/medium_api_gateway/genproto/post_service"
)

// @Security ApiKeyAuth
// @Router /likes [post]
// @Summary Create or update the like
// @Description Create a like
// @Tags like
// @Accept json
// @Produce json
// @Param like body models.CreateOrUpdateLikeRequest true "like"
// @Success 201 {object} models.Like
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateOrUpdateLike(c *gin.Context) {
	var (
		req models.CreateOrUpdateLikeRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	payload, err := h.GetAuthPayload(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	resp, err := h.grpcClient.LikeService().CreateOrUpdate(context.Background(), &pbl.Like{
		PostId: req.PostID,
		UserId: payload.UserID,
		Status: req.Status,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseLikeModel(resp))
}

func parseLikeModel(like *pbl.Like) models.Like {
	return models.Like{
		ID:     like.Id,
		PostID: like.PostId,
		UserID: like.UserId,
		Status: like.Status,
	}
}

// @Security ApiKeyAuth
// @Router /likes/{id} [get]
// @Summary Get the like by id
// @Description Get the like by post-id
// @Tags like
// @Accept json
// @Produce json
// @Param id path int true "post_id"
// @Success 200 {object} models.Like
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetLike(c *gin.Context) {
	postID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	payload, err := h.GetAuthPayload(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	like, err := h.grpcClient.LikeService().Get(context.Background(), &pbl.GetLike{
		UserId: payload.UserID,
		PostId: int64(postID),
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to get like")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, parseLikeModel(like))

}

// @Security ApiKeyAuth
// @Router /likes/post-likes/{id} [get]
// @Summary Get likes' and dislikes' count by post-id
// @Description Get likes' and dislikes' count by post-id
// @Tags like
// @Accept json
// @Produce json
// @Param id path int true "post_id"
// @Success 200 {object} models.LikesAndDislikesCount
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllLikesCount(c *gin.Context) {
	postID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	res, err := h.grpcClient.LikeService().GetAllLikesCount(context.Background(), &pbl.GetLike{PostId: postID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, models.LikesAndDislikesCount{
		Likes:    res.LikesCount,
		Dislikes: res.DislikesCount,
	})
}
