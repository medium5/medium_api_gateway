package v1

import (
	"context"
	"net/http"
	"strconv"

	"gitlab.com/medium5/medium_api_gateway/api/models"
	pbp "gitlab.com/medium5/medium_api_gateway/genproto/post_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// @Router /posts [post]
// @Summary Create a post
// @Description Create a post
// @Tags post
// @Accept json
// @Produce json
// @Param post body models.CreatePostRequest true "post"
// @Success 201 {object} models.Post
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreatePost(c *gin.Context) {
	var req models.CreatePostRequest

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	payload, err := h.GetAuthPayload(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	resp, err := h.grpcClient.PostService().Create(context.Background(), &pbp.Post{
		Title:       req.Title,
		Description: req.Description,
		ImageUrl:    req.ImageUrl,
		UserId:      payload.UserID,
		CategoryId:  req.CategoryID,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parsePostModel(resp))
}

func parsePostModel(post *pbp.Post) models.Post {
	return models.Post{
		ID:          post.Id,
		Title:       post.Title,
		Description: post.Description,
		ImageUrl:    post.ImageUrl,
		UserID:      post.UserId,
		CategoryID:  post.CategoryId,
		CreatedAt:   post.CreatedAt,
		ViewsCount:  post.ViewsCount,
	}
}

// @Security ApiKeyAuth
// @Router /posts/{id} [get]
// @Summary Get posts by id
// @Description Get posts by id
// @Tags post
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.Post
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetPost(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	resp, err := h.grpcClient.PostService().Get(context.Background(), &pbp.GetPost{Id: int64(id)})
	if err != nil {
		h.logger.WithError(err).Error("failed to get post")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	post := parsePostModel(resp)

	likesInfo, err := h.grpcClient.LikeService().GetAllLikesCount(context.Background(), &pbp.GetLike{PostId: resp.Id})
	if err != nil {
		h.logger.WithError(err).Error("failed to get likes' and dislikes' count")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	post.LikeInfo = models.PostLikeInfo{
		LikesCount:    likesInfo.LikesCount,
		DislikesCount: likesInfo.DislikesCount,
	}

	c.JSON(http.StatusOK, post)

}

// @Security ApiKeyAuth
// @Router /posts [get]
// @Summary Get all posts
// @Description Get all posts
// @Tags post
// @Accept json
// @Produce json
// @Param filter query models.GetAllPostsParams false "Filter"
// @Success 200 {object} models.GetAllPostsResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllPosts(c *gin.Context) {
	req, err := validateGetAllPostsParams(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.PostService().GetAll(context.Background(), &pbp.GetAllPostsRequest{
		Page:       req.Page,
		Limit:      req.Limit,
		Search:     req.Search,
		CategoryId: req.CategoryID,
		UserId:     req.UserID,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to get all posts")
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, getPostsResponse(result))

}

func getPostsResponse(data *pbp.GetAllPostsResponse) *models.GetAllPostsResponse {
	response := models.GetAllPostsResponse{
		Posts: make([]*models.Post, 0),
		Count: data.Count,
	}

	for _, post := range data.Posts {
		p := parsePostModel(post)
		response.Posts = append(response.Posts, &p)
	}

	return &response
}

// @Security ApiKeyAuth
// @Router /posts/{id} [put]
// @Summary Update a post
// @Description Update a post
// @Tags post
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param post body models.UpdatePostRequest true "Post"
// @Success 200 {object} models.Post
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdatePost(c *gin.Context) {
	var req models.Post

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	payload, err := h.GetAuthPayload(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}

	post, err := h.grpcClient.PostService().Update(context.Background(), &pbp.UpdatePostRequest{
		Id:          id,
		Title:       req.Title,
		Description: req.Description,
		ImageUrl:    req.ImageUrl,
		UserId:      payload.UserID,
		CategoryId:  req.CategoryID,
		UpdatedAt:   req.UpdatedAt,
	})
	if err != nil {
		h.logger.WithError(err).Error("failed to update post")
		if s, _ := status.FromError(err); s.Code() == codes.NotFound {
			c.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, parsePostModel(post))
}

// @Security ApiKeyAuth
// @Router /posts/{id} [delete]
// @Summary Delete post
// @Description Delete post
// @Tags post
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeletePost(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	payload, err := h.GetAuthPayload(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	_, err = h.grpcClient.PostService().Delete(context.Background(), &pbp.DeletePost{Id: int64(id), UserId: payload.UserID})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, models.ResponseOK{
		Message: "Success",
	})
}
