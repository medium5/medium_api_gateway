package models

type Category struct {
	ID        int64  `json:"id"`
	Title     string `json:"title"`
	CreatedAt string `json:"created_at"`
}

type CreateCategoryRequest struct {
	Title string `json:"title"`
}

type GetAllCategoriesResponse struct {
	Categories []*Category `json:"categories"`
	Count      int32       `json:"count"`
}

type UpdateCategoryRequest struct {
	Title string `json:"title"`
}