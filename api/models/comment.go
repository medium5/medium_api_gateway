package models

type Comment struct {
	ID          int64  `json:"id"`
	UserID      int64  `json:"user_id"`
	PostID      int64  `json:"post_id"`
	Description string `json:"description"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
	User        CommentUser
}

type UpdateCommentRequest struct {
	Description string `json:"description" binding:"required"`
}

type CommentUser struct {
	FirstName       string `json:"first_name"`
	LastName        string `json:"last_name"`
	Email           string `json:"email"`
	ProfileImageUrl string `json:"profile_image_url"`
}

type CreateCommentRequest struct {
	PostID      int64  `json:"post_id" binding:"required"`
	Description string `json:"description" binding:"required"`
}

type GetAllCommentsRequest struct {
	Limit  int32 `json:"limit" binding:"required" default:"10"`
	Page   int32 `json:"page" binding:"required" default:"1"`
	UserID int64 `json:"user_id"`
	PostID int64 `json:"post_id"`
}
type GetAllCommentsResponse struct {
	Comments []*Comment `json:"comments"`
	Count    int32      `json:"count"`
}
